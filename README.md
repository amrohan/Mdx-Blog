

[![Netlify deploy](https://api.netlify.com/api/v1/badges/c6f44d34-0570-4ca0-9d3d-cabdaa2b3afb/deploy-status)](https://rohan.gq)

# Portfolio

A portfolio created with next js and netlify cms that can be used for both blogging and showcasing your project work. 




  


## Screenshot
[![Website Screenshot](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8d4valy2t8gkmz8e6tsp.png)](https://rohan.ml)

  ## Installation :

1. Clone this repo:
    ```bash
     git clone https://github.com/amrohan/Portfolio
    ```

2. Install all packages :

    `npm install or yarn install` 

3. Runs the app in the development mode.

    `npm start`

    Open `http://localhost:3000` to view it in the browser.
4. Make any changes you desire to the code.


## Deployment
1. Create netlify account / sign in
2. reate a new netlify site and link it to your GitHub repository.
3. After that enable identify in netlify site .
4. Then go to your deployment url :
> **_NOTE_** : &nbsp; Add `/admin` at the end of the deployed netlify site link
`https://{url netlify site url }/admin`

5. Then login with netlify and create I'd and password in your identify
6. Then log in to Netlify and establish a username and password for yourself.
7. Then you can easily upload content through it .
8.  Your site is up and running, and you can start writing right away.
## Support

For queries contact me\
[@amrohan](https://t.me/amrohan)

  
